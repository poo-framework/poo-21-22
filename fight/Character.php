<?php


abstract class Character implements Movable
{

    use Persistable;

    private string $name;
    private int $health;
    protected int $strenght = 10;
    protected MoveCoordinates $position;

    private static int $instanceNumber = 0; // Ceci est stocké au niveau de la classe, la valeur est identique pour toutes ces instances

    /**
     * Character constructor.
     * @param $name
     * @param $health
     */
    public function __construct($name, $health)
    {
        $this->setName($name);
        $this->setHealth($health);
        self::$instanceNumber ++;
        $this->position = new  MoveCoordinates(0,0,0);
        Logger::log("Nouveau combattant dans l'arène, il y a désormais ".self::$instanceNumber." combattants");
    }

    public function hit(Character $victim): void {
        Logger::log($this->getName()." veut taper ".$victim->getName());
        $victim->takeHit($this->strenght);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @param int $health
     */
    public function setHealth(int $health): void
    {
        if ($health > 100 || $health < 0) {
            throw new Exception("health should be between 0 and 100, ".$health." given");
        }
        $this->health = $health;
    }

    /**
     * @return int
     */
    public static function getInstanceNumber(): int
    {
        return self::$instanceNumber;
    }

    abstract public function takeHit(int $strength): void;


    public function getPosition(): MoveCoordinates
    {
        return $this->position;
    }
}