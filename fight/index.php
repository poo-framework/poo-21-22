<?php
require_once "AutoLoad.php";
spl_autoload_register("AutoLoad::loadClass");

$manager = new CharacterManager(new DBConnector('poo_21_22', 'root', 'root', '3306', '127.0.0.1'));

$perso1 = new Ninja("Law", 100);
$perso2 = new Bear("Kuma", 100);

$manager->create($perso1);
$manager->create($perso2);

Mover::moveElement($perso1, new MoveCoordinates(10,10,10));
Mover::moveElement($perso1, new MoveCoordinates(10,10,0));
Mover::moveElement($perso1, new MoveCoordinates(10,20,0));
Mover::moveElement($perso2, new MoveCoordinates(10,100,0));


$perso2->hit($perso1);
$perso1->hit($perso2);
$perso2->hit($perso1);
$perso2->hit($perso1);
$perso1->hit($perso2);

$manager->update($perso1);
$manager->update($perso2);


