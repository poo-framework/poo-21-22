<?php

class CharacterManager
{

    private DBConnector $connector;

    const TABLE_NAME = 'characters';
    const TYPE_NINJA = 'ninja';
    const TYPE_BEAR = 'bear';

    /**
     * CharacterManager constructor.
     * @param DBConnector $connector
     */
    public function __construct(DBConnector $connector)
    {
        $this->connector = $connector;
    }

    public function getCharacters(): array {
        $characters = [];
        // Ici il faudra faire de la popote
        return $characters;
    }

    public function getCharacter(int $id): Character {
        $characters = new Ninja('ninja', 100);
        // Ici il faudra faire de la popote
        return $characters;
    }

    public function create(Character $character): Character {
        $request = 'INSERT INTO '.self::TABLE_NAME.' (`name`,`health`,`type`) values (:name, :health, :type)';
        $connection = $this->connector->getConnection();
        $req = $connection->prepare($request);

        $name = $character->getName();
        $req->bindParam('name', $name);

        $health = $character->getHealth();
        $req->bindParam('health', $health);

        $type = $character instanceof Ninja ? self::TYPE_NINJA : self::TYPE_BEAR;
        $req->bindParam('type', $type);

        $req->execute();

        $character->setId($connection->lastInsertId());

        return $character;
    }

    public function update(Character $character): Character {
        $characters = new Ninja('ninja', 100);

        // Ici il faudra faire de la popote
        return $character;
    }
}