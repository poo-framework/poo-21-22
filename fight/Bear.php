<?php

class Bear extends Character
{
    private const BASE_STRENGTH = 20; // Un constante est toujours statique, une constante ne peut jamais changer

    public function __construct($name, $health)
    {
        parent::__construct($name, $health);
        $this->strenght = self::BASE_STRENGTH;
    }

    public function takeHit(int $strength): void
    {
        $this->setHealth($this->getHealth() - $strength);
        Logger::log($this->getName()." a pris un coup, sa nouvelle santé est de ".$this->getHealth());
    }

    public function move(MoveCoordinates $coordinates)
    {
        Logger::log($this->getName().' se déplace un peu bruyamment vers la position '.$this->position->getX().','.$this->position->getY().','.$this->position->getZ());
        Logger::log('............');
        $this->position = $coordinates;
        Logger::log($this->getName().' a fini de se déplacer');
    }
}