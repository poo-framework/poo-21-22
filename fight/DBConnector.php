<?php


class DBConnector
{

    public static bool $debug = false;

    private string $dbName;
    private string $user;
    private string $pass;
    private string $port;
    private string $host;

    /**
     * DBConnector constructor.
     * @param string $dbName
     * @param string $user
     * @param string $pass
     * @param string $port
     * @param string $host
     */
    public function __construct(string $dbName, string $user, string $pass, string $port, string $host)
    {
        $this->dbName = $dbName;
        $this->user = $user;
        $this->pass = $pass;
        $this->port = $port;
        $this->host = $host;
    }

    public function getConnection(): PDO {
        $pdo = new PDO("mysql:host=".$this->host.';dbname='.$this->dbName, $this->user, $this->pass);
        if (self::$debug) {
            $pdo->setAttributes(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $pdo;
    }


}