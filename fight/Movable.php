<?php

interface Movable
{
    public function move(MoveCoordinates $coordinates);

    public function getPosition(): MoveCoordinates;
}