<?php


class Mover
{

    public static function moveElement(Movable $movable, MoveCoordinates $coordinates) {
        $movable->move($coordinates);
    }

}