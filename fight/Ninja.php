<?php


class Ninja extends Character
{
    public function takeHit(int $strength): void
    {
        $random = rand(0,2);
        if ($random === 0) {
            $this->setHealth($this->getHealth() - $strength);
            Logger::log($this->getName()." a pris un coup, sa nouvelle santé est de ".$this->getHealth());
        }
        Logger::log($this->getName()." a esquivé le coup, sa santé reste de ".$this->getHealth());
    }

    public function move(MoveCoordinates $coordinates)
    {
        Logger::log($this->getName().' disparait à la position '.$this->position->getX().','.$this->position->getY().','.$this->position->getZ());
        $this->position = $coordinates;
        Logger::log($this->getName().' réapparait à la position '.$this->position->getX().','.$this->position->getY().','.$this->position->getZ());
    }
}