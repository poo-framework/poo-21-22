<?php


class Personnage
{
    public $name;
    public $age;

    public function present() {
        echo "Bonjour, je suis ".$this->name." et j'ai ".$this->age." ans";
    }

    public function saySomething(string $something): void {
        echo $this->name.': '.$something.'<br>';
    }
}